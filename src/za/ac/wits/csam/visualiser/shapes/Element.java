package za.ac.wits.csam.visualiser.shapes;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Steve on 26-Oct-17.
 */
public class Element extends Rectangle {

    public Element(int x, int y, Color colour) {
        super(x * 10, y * 10, 10, 10);
        getStyleClass().add("element");
        setFill(colour);
    }
}

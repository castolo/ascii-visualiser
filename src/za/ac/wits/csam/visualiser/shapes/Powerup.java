/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.ac.wits.csam.visualiser.shapes;

import javafx.scene.shape.Circle;

/**
 * A shape for a power-up
 *
 * @author Jeremy Laihong
 * @author Steve James
 */

public class Powerup extends Circle {

    public Powerup(int x, int y) {
        double radius = 5;
        setCenterX(x * 10 + radius);
        setCenterY(y * 10 + radius);
        setRadius(radius);
        getStyleClass().add("powerup");
    }
}

package za.ac.wits.csam.visualiser;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import za.ac.wits.csam.visualiser.shapes.Element;
import za.ac.wits.csam.visualiser.shapes.Powerup;

/**
 * The type of elements on the board
 *
 * @author Steve James
 */
public class Type {

    private static final char EMPTY = ' ';
    private static final char OBSTACLE = '#';
    private static final char POWERUP = '*';
    private static final char TRACE = '-';

    private static final Color[] AGENT_COLOURS = {Color.RED, Color.CORNFLOWERBLUE, Color.LIME, Color.YELLOW};

    private final char code;


    public Type(char code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Type type = (Type) o;

        return code == type.code;
    }

    @Override
    public int hashCode() {
        return (int) code;
    }

    public boolean isEmpty() {
        return code == EMPTY;
    }

    public static boolean isValid(char code) {
        return Character.isDigit(code) || code == EMPTY || code == OBSTACLE || code == TRACE || code == POWERUP;
    }

    public Shape toShape(int x, int y) {
        switch (code) {
            case POWERUP:
                return new Powerup(x, y);
            case OBSTACLE:
                return new Element(x, y, Color.BLACK);
            case TRACE:
                return new Element(x, y, Color.WHITE);
            default:
                return new Element(x, y, AGENT_COLOURS[((code - '0') - 1) % AGENT_COLOURS.length]);
        }
    }

}

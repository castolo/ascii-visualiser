package za.ac.wits.csam.visualiser;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * The ASCII representation of the board
 *
 * @author Steve James
 */
public class Board {

    private final int columns;
    private final int rows;
    private final Type[][] board;

    public static final Pane PANE = new Pane();


    public Board(String board) throws InvalidBoardFormat {
        String[] arr = board.split("\n");
        rows = arr.length;
        if (rows == 0) {
            throw new InvalidBoardFormat("Empty board");
        }
        columns = arr[0].length();
        if (rows != columns) {
            throw new InvalidBoardFormat("Non-square board");
        }
        this.board = new Type[rows][columns];
        for (int i = 0; i < rows; i++) {
            char[] row = arr[i].toCharArray();
            for (int j = 0; j < columns; j++) {
                if (!Type.isValid(row[j])) {
                    throw new InvalidBoardFormat("Invalid character " + row[j]);
                }
                this.board[i][j] = new Type(row[j]);
            }
        }
    }

    public static List<Board> load(InputStream is) throws IOException {

        List<Board> boards = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            if (line.isEmpty()) {
                boards.add(new Board(sb.toString().trim()));
                sb.setLength(0);
            } else {
                sb.append(line).append('\n');
            }
        }
        if (sb.length() > 0) {
            boards.add(new Board(sb.toString().trim()));
        }
        return boards;
    }

    public List<Shape> toShapes() {

        List<Shape> shapes = new ArrayList<>(rows * columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                Type type = board[i][j];
                if (!type.isEmpty())
                    shapes.add(type.toShape(j, i));
            }
        }
        return shapes;
    }

    public synchronized BufferedImage toImage() {
        PANE.getChildren().clear();
        PANE.getChildren().addAll(toShapes());
        WritableImage writableImage = new WritableImage(510, 510);
        PANE.snapshot(null, writableImage);
        BufferedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
        return renderedImage;
    }

}

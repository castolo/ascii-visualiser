/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.wits.csam.visualiser;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Steve James
 */
public class Visualiser extends Application {

    protected static final URL CSS_URL = Visualiser.class.
            getResource("main.css");

    @Override
    public void start(Stage stage) throws Exception {

        List<String> params = getParameters().getRaw();
        if (params.size() != 2) {
            System.err.println("Expected two arguments: <path/to/states/file> <image/output/directory>");
            Platform.exit();
            return;
        }

        Pane pane = Board.PANE; //HACK
        pane.getStyleClass().add("root");


        Scene scene = new Scene(pane);
        scene.getStylesheets().addAll(CSS_URL.toExternalForm());
        stage.setScene(scene);
        //stage.show();

        List<Board> boards = Board.load(new FileInputStream(params.get(0)));
        List<BufferedImage> images = boards.stream()
                .map(Board::toImage)
                .collect(Collectors.toList());
        toImages(images, new File(params.get(1)));
        // then use ffmpeg -r 20 -f image2 -s 510x510 -start_number 0 -i %d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p test.mp4
        Platform.exit();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        launch(args);
    }

    /**
     * Write the individual images to file
     *
     * @param images    the images
     * @param directory the directory storing the images
     * @throws IOException
     */
    public void toImages(List<BufferedImage> images, File directory) throws IOException {
        directory.mkdirs();
        for (int i = 0; i < images.size(); i++) {
            System.err.println("Writing image " + (i + 1));
            File file = new File(directory, i + ".png");
            ImageIO.write(images.get(i), "png", file);
        }
    }

    /**
     * Create a GIF from the images
     *
     * @param images the images
     * @param file   the file to write to
     * @throws IOException
     * @deprecated I suggest not using this, since it won't do any compression. You'll probably end up with a +-40MB file
     */
    public void toGif(List<BufferedImage> images, File file) throws IOException {
        BufferedImage firstImage = images.get(0);
        ImageOutputStream output = new FileImageOutputStream(file);
        GifSequenceWriter writer =
                new GifSequenceWriter(output, firstImage.getType(), 50, false);
        writer.writeToSequence(firstImage);
        for (int i = 1; i < images.size() - 1; i++) {
            writer.writeToSequence(images.get(i));
        }
        writer.close();
        output.close();
    }


}

package za.ac.wits.csam.visualiser;

/**
 * An exception to be thrown when an invalid board state is received
 *
 * @author Dean Wookey
 */
public class InvalidBoardFormat extends RuntimeException {

    public InvalidBoardFormat(String message) {
        super(message);
    }
}

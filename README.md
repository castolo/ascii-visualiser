USAGE: java -jar <file.jar> <path/to/states/file> <image/output/directory>

A small project for creating a video from a list of ASCII boards.

Based on work by Dean Wookey and Jeremy Laihong (https://bitbucket.org/Gorthaur/snakevisualiser)